<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> id="html" class="no-js <?php if ( is_front_page() ) { echo 'ccss'; } ?>" >
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title(); ?></title>
<?php wp_head(); ?>

<!-- Styles -->

<?php if(SITE_COMPRESSION) : ?>

	<?php if ( is_front_page() ) : ?>

		<noscript><link rel="stylesheet" href="<?php echo cache_bust( 'assets/css/app.min.css'); ?>"></noscript>
		<?php include(TPL_DIR . '/inc/load-css.php'); ?>
		<script>
			var stylesheet = loadCSS( "<?php echo cache_bust( 'assets/css/app.min.css'); ?>" );
			onloadCSS( stylesheet, function() {
			  document.getElementById("html").className =
		    document.getElementById("html").className.replace(/\bccss\b/,'');
			});
		</script>
		<style type="text/css">
			<?php echo str_replace ( 'url("..', 'url("' . ASS_URI, str_replace ( 'url(..', 'url(' . ASS_URI , file_get_contents( get_template_directory() . '/assets/css/critical.min.css') ) ); ?>
		</style>
		<script></script><!-- here to ensure a non-blocking load still occurs in IE and Edge, even if scripts follow loadCSS in head -->

	<?php else : ?>
		
		<link rel="stylesheet" href="<?php echo cache_bust( 'assets/css/app.min.css') ; ?>">
		
	<?php endif; ?>

<?php else : ?>

	<link rel="stylesheet" type="text/css" media="all" href="<?php echo ASS_URI . '/css/app.css'; ?>">

<?php endif; ?>

<!-- /Styles -->

<?php include(TPL_DIR . '/inc/font-script.php'); ?>

<link rel="apple-touch-icon" sizes="180x180" href="<?php echo ASS_URI; ?>/img/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="<?php echo ASS_URI; ?>/img/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php echo ASS_URI; ?>/img/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="<?php echo ASS_URI; ?>/img/favicon/manifest.json">
<link rel="shortcut icon" href="<?php echo ASS_URI; ?>/img/favicon/favicon.ico">
<meta name="msapplication-config" content="<?php echo ASS_URI; ?>/img/favicon/browserconfig.xml">
<meta name="theme-color" content="#ffffff">

</head>

<body <?php body_class('site'); ?>>
	
	<?php /*
	<!-- <header id="header" class="site-header">
		Header
	</header> -->
	<!-- .site-header -->
	*/ ?>
	
	<div class="site-content">
		
	<?php include ('parts/page-header.php'); ?>
