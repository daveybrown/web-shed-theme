<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

?>

</div>
<!-- /.site-content -->

<footer id="footer" class="site-footer text-center">
	<!-- hello i'm the footer -->
  
  <p>
    <strong>
      Bush strokes <?php if ( is_front_page() ) { echo '&amp; tea cup '; } ?> <a href="http://www.freepik.com" style="text-decoration: none;">designed by freepik</a><br>
      Website designed &amp; built by Davey Brown &copy; Web Shed <?php echo auto_copyright(2016); ?>
    </strong>
  </p>
  
</footer>


<script type='text/javascript'>
/* <![CDATA[ */
var _wpcf7 = {"loaderUrl":"<?php echo str_replace('/', '\/', ASS_URI); ?>\/img\/loader.svg","recaptcha":{"messages":{"empty":"Please verify that you are not a robot."}},"sending":"Sending ..."};
/* ]]> */
</script>

<?php wp_footer(); ?>

<!-- Scripts -->

<?php if(!SITE_COMPRESSION) : ?>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/foundation.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/what-input.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/src/highlight.pack.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/antiscroll.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.ba-throttle-debounce.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/fastclick.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/tooltipster.bundle.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/src/jquery.lazyfish.js"></script>

<script type="text/javascript" src="<?php echo plugins_url(); ?>/contact-form-7/includes/js/jquery.form.min.js"></script>
<script type="text/javascript" src="<?php echo plugins_url(); ?>/contact-form-7/includes/js/scripts.js"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/src/app.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/assets/js/src/tooltips.js"></script>

<?php endif; ?>

<!-- /Scripts -->


</body>
</html>
