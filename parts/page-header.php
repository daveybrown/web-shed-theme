<?php $bg_img = get_field('page_background', 'options');

// dbug($bg_img);

 ?>

<section class="intro">
  
  <style>
    .bg--intro {
      background-image: url(<?php echo $bg_img['sizes']['large']; ?>);
    }
    @media screen and (min-width: 40em) {
      .bg--intro {
        background-image: url(<?php echo $bg_img['url']; ?>);
      }
    }
  </style>
  
  <div class="bg bg--cover bg--intro"></div>
  
  <div class="middle">
    <div class="middled text-center">
      
      <div class="headers">
        <h1><a href="<?php echo HME_URL; ?>">Web-shed</a></h1>
        <div class="clear"></div>
        <span class="subheader h">Solar powered web</span>
      </div>
      
      
      
      <div class="row row--menu show-for-medium">
        <div class="small-12 columns text-center">
          <div class="wrap--list">
            <ul class="nav--horizontal h">
              <li class="nav-item" id="a1"><a href="<?php echo HME_URL; ?>#about-me">About Me</a></li>
              <li class="nav-item" id="a2"><a href="<?php echo HME_URL; ?>#portfolio">Portfolio</a></li>
              <li class="nav-item" id="a3"><a href="<?php echo HME_URL; ?>#toolbox">Toolbox</a></li>
              <li class="nav-item" id="a3"><a href="<?php echo HME_URL; ?>#blog">Blog</a></li>
              <li class="nav-item" id="a4"><a href="<?php echo HME_URL; ?>#contact-me">Contact me</a></li>
              <li class="nav-more" id="a5">
                <a class="button--more" href="">Menu</a>
                <ul class="nav--vertical h">
                  <li class=""><a href="#about-me">What I do</a></li>
                  <li class=""><a href="#portfolio">Portfolio</a></li>
                  <li class=""><a href="#toolbox">Toolbox</a></li>
                  <li class=""><a href="#contact-me">Contact me</a></li>
                </ul>
              </li>
            </ul>
            
            
          </div>
        </div>
      </div>
      
      
      
    </div>
  </div>
  
  
</section>	