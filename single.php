<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package webshed
 */

$fileds = get_fields();

get_header(); ?>	
	
	<?php include ('inc/modules.php'); ?>

<?php
get_footer();
