<?php
/*
  4 Ooh 4
 */
get_header();
?>

	<?php $wp_query = new WP_Query( array( 'page_id' => get_field('custom_404_page', 'option') ) ); ?>

	<?php

	get_header(); ?>
		
		<section class="intro">
			
			<div class="bg bg--cover bg--intro"></div>
			
		</section>	
		
		<?php include ('inc/modules.php'); ?>
	
	<?php wp_reset_query(); ?>

<?php get_footer();