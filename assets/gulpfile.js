var gulp = require('gulp');

// note that by default gulp-load-plugins will only work with NPM packages that are prefixed with 'gulp-'
var $ = require('gulp-load-plugins')({
      pattern: '*'
  });

var sassPaths = [
  'bower_components/foundation-sites/scss',
  'bower_components/motion-ui/src',
  'bower_components/highlight-js/src/styles',
  "bower_components/nanoscroller/bin/css",
  "bower_components/antiscroll",
  "node_modules/tooltipster/dist/css",
  '../../../plugins/contact-form-7/includes/css',
  '../components'
];

gulp.task('sass', function() {
  return gulp.src(['scss/*.scss', '!scss/_*.scss'])
    .pipe($.sourcemaps.init())
    .pipe($.plumber())
    .pipe($.sass({
      includePaths: sassPaths,
      outputStyle: 'compact' // compact (compressed breaks sourcemaps)
    })
    .on('error', $.sass.logError))
    .pipe($.autoprefixer({
      browsers: ['last 4 versions', 'ie >= 9']
    }))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('css'))
    .pipe($.browserSync.stream());
});

gulp.task('default', ['serve']);

// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function() {

    $.browserSync.init({
        proxy: "web-shed.local"
    });
    gulp.watch(["scss/**/*.scss","../components/*/*.scss"], ['sass']);
    gulp.watch("../**/*.php").on('change', $.browserSync.reload);
    gulp.watch(["js/src/**/*.js"]).on('change', $.browserSync.reload);
    gulp.watch("./exportfiles.json", ['concat-js']);
    gulp.watch(["./../acf-json/group_*.json", "./../components/**/*.json"], ['acf']);
});

/* 
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
** Build task
** Concatinate js according to exportfiles.json, then minify, then gzip
** Compile sass to css, then minfy, then gzip
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
*/


gulp.task('build', ['concat-js', 'minify-js', 'compress-js', 'sass', 'minify-css', 'compress-css'] );

gulp.task('concat-js', function(){
    purgeCache('./exportfiles.json');
    var exportfiles = require('./exportfiles.json');
    gulp.src(exportfiles.devScripts)
        .pipe(gulp.dest('js'));
    gulp.src(exportfiles.prodScriptsFonts)
        .pipe($.concat('export-fonts.js'))
        .pipe(gulp.dest('js/dist'));
    gulp.src(exportfiles.prodScriptsLoadCSS)
        .pipe($.concat('export-loadCSS.js'))
        .pipe(gulp.dest('js/dist'));
    return gulp.src(exportfiles.prodScripts)
        .pipe($.concat('export.js'))
        .pipe(gulp.dest('js/dist'));
});

gulp.task('minify-js', ['concat-js'], function (cb) {
  $.pump([
        gulp.src(['js/dist/*.js', '!js/dist/*.min.js']),
        $.uglify(),
        $.rename({
            suffix: '.min'
        }),
        gulp.dest('js/dist')
    ],
    cb
  );
});

gulp.task('compress-js', ['minify-js'], function(){
    return gulp.src('js/dist/*.min.js')
        .pipe($.gzip())
        .pipe(gulp.dest('js/dist'))
});

gulp.task('minify-css', ['sass'], function(){
  return gulp.src(['css/*.css', '!css/*.min.css'])
    .pipe($.uglifycss({
      "maxLineLen": 80,
      "uglyComments": true
    }))
    .pipe($.rename({
        suffix: '.min'
    }))
    .pipe(gulp.dest('css'));
});

// Compress css, define minify css as a dependant task
gulp.task('compress-css', ['minify-css'], function(){
  return gulp.src('css/*.min.css')
    .pipe($.gzip())
    .pipe(gulp.dest('css'));
});

/* 
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
** End of build task
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
*/

/* 
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
** Purge and search cache functions
** Used to clear cached exportfiles.json
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
*/

/**
 * Removes a module from the cache
 */
function purgeCache(moduleName) {
    // Traverse the cache looking for the files
    // loaded by the specified module name
    searchCache(moduleName, function (mod) {
        delete require.cache[mod.id];
    });

    // Remove cached paths to the module.
    // Thanks to @bentael for pointing this out.
    Object.keys(module.constructor._pathCache).forEach(function(cacheKey) {
        if (cacheKey.indexOf(moduleName)>0) {
            delete module.constructor._pathCache[cacheKey];
        }
    });
};
/**
 * Traverses the cache to search for all the cached
 * files of the specified module name
 */
function searchCache(moduleName, callback) {
    // Resolve the module identified by the specified name
    var mod = require.resolve(moduleName);

    // Check if the module has been resolved and found within
    // the cache
    if (mod && ((mod = require.cache[mod]) !== undefined)) {
        // Recursively go over the results
        (function traverse(mod) {
            // Go over each of the module's children and
            // traverse them
            mod.children.forEach(function (child) {
                traverse(child);
            });

            // Call the specified callback providing the
            // found cached module
            callback(mod);
        }(mod));
    }
};

/* 
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
** End of purge and search cache functions
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
*/


/* 
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
** Build web fonts from fonts/src
** This task may require a 'brew install fontforge'
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
*/

gulp.task('fontgen', function() {
  return gulp.src("fonts/src/**/*.{ttf,otf}")
    .pipe($.fontgen({
      dest: "fonts/"
    }));
});


/* 
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
** Google page speed insights
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
*/

// for now i can run
// ngrok http 3000
// psi xxx 

// var gulp = require('gulp');
// var psi = require('psi');
// var site = 'http://www.html5rocks.com';
// var key = 'AIzaSyCWOdVIR8vxZY50FN0wPbnSLyLD2vyiVAs';
// 
// // Please feel free to use the `nokey` option to try out PageSpeed
// // Insights as part of your build process. For more frequent use,
// // we recommend registering for your own API key. For more info:
// // https://developers.google.com/speed/docs/insights/v2/getting-started
// 
// gulp.task('mobile', function () {
//     return psi(site, {
//         key: key,
//         strategy: 'mobile',
//     }).then(function (data) {
//         console.log('Speed score: ' + data.ruleGroups.SPEED.score);
//         console.log('Usability score: ' + data.ruleGroups.USABILITY.score);
//     });
// });
// 
// gulp.task('desktop', function () {
//     return psi(site, {
//         key: key,
//         strategy: 'desktop',
//     }).then(function (data) {
//         console.log('Speed score: ' + data.ruleGroups.SPEED.score);
//     });
// });
// 
// gulp.task('psia', ['mobile']);

var GulpSSH = require('gulp-ssh')
var ssh = require('./ssh.json');

var config = {
  host: 'shell.gridhost.co.uk',
  port: 22,
  username: 'webshedc',
  password: ssh.pw
  // privateKey: fs.readFileSync('/Users/zensh/.ssh/id_rsa')
}
 
var gulpSSH = new GulpSSH({
  ignoreErrors: false,
  sshConfig: config
})

gulp.task('deploy-assets', function () {
  return gulp
    .src(['./**/*', '!**/node_modules/**', '!**/bower_components/**', '!**/ssh.json', '!**/*.scss', '!**/fonts/**'])
    .pipe(gulpSSH.dest('public_html/wp-content/themes/webshed/assets'))
})

gulp.task('deploy-fonts', function () {
  return gulp
    .src(['./**/fonts/**/*'])
    .pipe(gulpSSH.dest('public_html/wp-content/themes/webshed/assets'))
})

gulp.task('deploy-theme', function () {
  return gulp
    .src(['./../**/*', '!./**'])
    .pipe(gulpSSH.dest('public_html/wp-content/themes/webshed'))
})

gulp.task('deploy-wp-admin', function () {
  return gulp
    .src(['./../../../../wp-admin/**/*'])
    .pipe(gulpSSH.dest('public_html/wp-admin'))
})


/* 
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
**  ACF components
** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 
*/

// Reading list:
// https://www.advancedcustomfields.com/resources/local-json/
// https://www.advancedcustomfields.com/resources/synchronized-json/

gulp.task('acf', function() {
  // purgeCache(jsoncombine);
  // console.log(data);
  gulp.src(['./../components/**/*.json', '!./../components/acf_empty_group.json'])
  .pipe($.jsoncombine("dist_group.json",function(data){
      
      purgeCache('./../components/acf_empty_group.json');
      var obj = require('./../components/acf_empty_group.json');
      
      Object.keys(data).forEach(function(key) {
        obj.fields[0].layouts.push(data[key]);
      });
      
      // todo check if changed
      obj.modified = Math.floor(new Date() / 1000);
      
      return new Buffer(JSON.stringify(obj));
   }))
   .pipe(gulp.dest("./../acf-json"));
});


