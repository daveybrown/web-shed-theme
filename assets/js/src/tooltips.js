$(document).ready(function() {            
  $('.toolbox .has--tooltip').tooltipster({
    theme: ['tooltipster-borderless', 'tooltipster-borderless-customized'],
    functionAfter: function(instance, helper){
      var tooltipContent = $(instance._$origin[0]).attr('data-tooltip-content');
      var dataShow = parseInt( $(tooltipContent).attr('data-show') ) + 1;
      if ( dataShow >= $(tooltipContent + ' .tip--part').length ) {
        dataShow = 0;
      }
      $(tooltipContent).attr('data-show', dataShow);
    },
    functionBefore: function(instance, helper){
      var tooltipContent = $(instance._$origin[0]).attr('data-tooltip-content');
      var content = $(tooltipContent);
      instance.content(content);
      // Close any other open tooltip
      $('.toolbox').find('.tooltipstered').tooltipster('close');
    },
    // trigger: 'click',
    contentCloning: true,
    trigger: 'custom',
    triggerOpen: {
        touchstart: true,
        mouseenter: true
    },
    triggerClose: {
        touchstart: true,
        mouseleave: true 
    }
  });  
  
  setTimeout(function(){ 
    randomToolTip()
  }, 3000);
  
});

function randomToolTip() {
  if ( $('.tooltipster-base').length == 0) {
    // random number for elements with tooltips starting at 0
    var random = Math.floor((Math.random() * $('.toolbox .has--tooltip').length));
    $('.toolbox .has--tooltip').eq(random).tooltipster('open');
  }
}