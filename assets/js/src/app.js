$(document).foundation();

$(document).ready(function() {
  
  styleCodeBlocks();
  // initSvgs(); 
  FastClick.attach(document.body);
  
  if($(window).width() > 820) {
    // console.log('not mobile');
    // introMenu();    
    textSwitch();    
    // automattic.github.io/antiscroll/
    $('.antiscroll-wrap').antiscroll({});
  } else {
    // is mobile
    // console.log('is mobile');
  }
  
  $(window).resize( $.throttle( 400, resizeThrottled ));
  
  function resizeThrottled() {
    $('.antiscroll-inner').attr('style', '');
    $('.antiscroll-wrap').antiscroll({});
    // $('.tooltipstered').tooltipster('close');
  }  
  
  if ( $.isFunction($.fn.lazyfish) && $('.lazy').length > 0 ) {
    $().lazyfish();
  }
  
  $('[data-toggle-class]').click(function() {
    $e = $(this);
    $t = $( $e.attr('data-target') );
    $t.toggleClass( $e.attr('data-toggle-class') );
  });
  
  $('.nav-more').click(function() {
    $('.nav--vertical').toggleClass('show');
    return false;
  });

  
  
});


function is_touch_device() {
  return 'ontouchstart' in window        // works on most browsers 
      || navigator.maxTouchPoints;       // works on IE10/11 and Surface
};

// $(window).resize( $.throttle( 400, introMenu ));

function introMenu() {

  // Reset classes
  $('.intro .nav--horizontal li.nav-item.show-for-sr').removeClass('show-for-sr');  
  $('.nav-more').removeClass('show');
  $('.intro .nav--vertical li').removeClass('show');
  
  var counter = 0;
  
  $($('.intro .nav--horizontal li.nav-item').get().reverse()).each(function() {
    
    counter = counter + 1;
    // console.log(counter);
    
    $e = $(this);
    $f = $('.intro .nav--horizontal li.nav-item').first();
    
    // If not all nav-items are at top, then show more button
    var posE = $e.position();
    var posF = $f.position();
    // console.log('e' + posE.top);
    // console.log('f' + posF.top);
    if( posE.top == posF.top ) {
      
      if( $('.nav-more.show').length > 0 ) {
        var $m = $e.siblings('.nav-more.show');
        var posM = $m.position();
        if( posM.top == posF.top ) {
          return false;
        }
        
        $e.addClass('show-for-sr'); 
        
        $('.intro .nav--vertical li').eq(4 - counter).addClass('show');
        
        // console.log('more' + posM.top);
        
        if( posM.top == 0 ) {
          return false;
        }
        
      }
      
    } else {
      $('.intro .nav--vertical li').eq(4 - counter).addClass('show');
      $e.addClass('show-for-sr');
      $e.siblings('.nav-more').addClass('show');
    } 
    
  });
  
}



function textSwitch() {

  $('.text-switch strong[data-switch]').each(function(i, block) {
    
    var text = $(this).text();
    
    var a = text.split("/");
    var index;
    var maxWidth = 0;
    var maxWord = '';
    var html = '';
    for (index = 0; index < a.length; ++index) {
        $(this).text( a[index] );
        var width = $(this).width();
        var word = $(this).text();
        if( width > maxWidth ) {
          maxWidth = width;
          maxWord = word;    
        } 
    }
    
    for (index = 0; index < a.length; ++index) {
        if(a[index] == maxWord) {
          html = html + '<span class="word word--'+ (index + 1) +' word--max">'+a[index]+'</span>';
        } else {
          html = html + '<span class="word word--'+ (index + 1) +'">'+a[index]+'</span>';
        }
        if(index + 1 < a.length) {
          html = html + '<span class="show-for-sr">/</span>';
        }
    }
    
    $(this).text(maxWord);
    $(this).append(html).attr('data-count-state', a.length).attr('data-state', 1);
    
    
  });
  
  makeSwitch();
  
  $('.text-switch').mouseenter(function(){
    $('html').attr('data-switch-pause', 'true')
  }).mouseleave(function(){
      $('html').removeAttr('data-switch-pause');
  });
  
  
}


function makeSwitch() {
  // console.log ( $('[data-switch]').length );
  var countSwitch = 0;
  $('[data-switch]').each(function(i, block) {
    if ($(this).attr('data-switch') > countSwitch) {
      countSwitch = parseInt($(this).attr('data-switch'));
    }
  });
  var switchTime = 3000;
  // console.log(countSwitch);
  var intervalTime = switchTime * (countSwitch + 1);
  // console.log(intervalTime);
  
  
  if ($('[data-switch-interval]').length == 0) {
    setInterval(function() { makeSwitch(); }, (intervalTime));
    $('html').attr('data-switch-interval', intervalTime);
  }
  
  // console.log(countSwitch);
  for (index = 1; index <= countSwitch; ++index) {
    
    
    
    var currentState = $('[data-switch="'+index+'"]').attr('data-state');
    var nextState = parseInt(currentState) + 1;
    
    if (index == 1) {    
      // console.log( 'n/s' + nextState );
      // console.log( 'm/s' + parseInt( $('[data-switch="'+index+'"]').attr('data-count-state') ));
    }    
    if ( nextState >  parseInt( $('[data-switch="'+index+'"]').attr('data-count-state') ) ) {
      nextState = 1;
    }
    setTimeout(function(index, nextState){ 
      if ($('[data-switch-pause]').length == 0) {
        $('[data-switch="'+index+'"]').attr('data-state', nextState);
      }
    }, switchTime * index, index, nextState);
  } 
  
  
  
}

/*
 * Style code blocks
 */

function styleCodeBlocks() { 
  $('pre code').each(function(i, block) {
    // pack is built at https://highlightjs.org/download/
    // check scss, css, js, php, json , md, html
    hljs.highlightBlock(block);
  });
  $('.editor-mockup pre code').each(function(i, block) {
    $el = $(this);
    var widthSpan = $el.find('> span').outerWidth();
    var paddCode = parseInt($el.css('padding-left')) + parseInt($el.css('padding-right'));
    $el.closest('pre').css('width', widthSpan + paddCode);    
  });
}

/*
 * Replace all SVG images with inline SVG
 */
 


// form here https://github.com/funkhaus/style-guide/blob/master/template/js/site.js#L32-L90
// and http://stackoverflow.com/a/11978996/6671505
function initSvgs() { 
 
// init global cache object and assign local var
        var cache = this.svgCache = this.svgCache || {};

        // Set total and counter
        var $svgs = jQuery('img.svg');
        var total = $svgs.length;
        var count = 0;

        // If no SVGs on page, fire callback event
        if ( total === count ) jQuery(document).trigger('svgsLoaded', [count]);

        // define function to replace single svg
        var replaceSVG = function( data ){

            // get img and attributes
            var $img = jQuery(this),
                attributes = $img.prop("attributes");

			// Increment counter
			count++;

            // Clone the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg').clone();

            // Remove any invalid XML tags as per http://validator.w3.org
            $svg = $svg.removeAttr('xmlns:a');

            // Loop through IMG attributes and add them to SVG
            jQuery.each(attributes, function() {
                $svg.attr(this.name, this.value);
            });

            // Replace image with new SVG
            $img.replaceWith($svg);

			// If this is the last svg, fire callback event
			if ( total === count ) jQuery(document).trigger('svgsLoaded', [count]);

        }

        // loop all svgs
        $svgs.each(function(){

            // get URL from this SVG
	        var imgURL = jQuery(this).attr('src');

            // if not cached, make new AJAX request
            if ( ! cache[imgURL] ){
                cache[imgURL] = jQuery.get(imgURL).promise();
            }

            // when we have SVG data, replace img with data
            cache[imgURL].done( replaceSVG.bind(this) );
            
            jQuery(this).closest('.brush--section').addClass('inlined');

		});
}
