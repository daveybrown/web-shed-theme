/*
 *  Project: Jquery LazyFish
 *  Version: 1.0
 *  Description: Simple lazy load image utility
 *  
 *  Author: http://kwaiu.com
 *  Licensed under the MIT license http://opensource.org/licenses/MIT
 */

;(function($, window, document, undefined){
	
	var pluginName = 'lazyfish', defaults = {speed: 0, part_load: false};		
	var offset = 0;
	
	function Plugin(element, options) {
		this.element = element; 
		this.options = $.extend({}, defaults, options) ;
		this.defaults = defaults; 
		this.name = pluginName; 
		this.init();
	}	
	Plugin.prototype = {
		
		init: function() {
			
			var _self = this;			
			
			_self.find_element();
      
      $(window).scroll( $.throttle( 250, function() { 
          offset = 400;
					_self.find_element(); 
        })
      );
      $(window).resize( $.throttle( 250, function() { 
          _self.find_element(); 
        })
      );
      		
			$('[data-do-lazy]').click(function() {
				_self.find_element();
				setTimeout(function(){ 
					_self.find_element();
				}, 40);
			});
			
		},
		
		find_element: function() {

			var _self = this;

			$('.lazy:not(.scrolled-to)').each(function() { 	
				
				if( _self.is_in_view($(this)) == true ) { 
					
					_self.image_load($(this));					

					$(this).addClass('scrolled-to');
				}	
				
				
				// load all
				// console.log('load all');
				// _self.image_load($(this));					
				// 
				// $(this).addClass('scrolled-to');
				
	
			});
		},
		
		image_load: function (element) { 
			var src = element.find('[data-src]').attr('data-src');				
			element.find('[data-src]').attr('src', src).one("load", function() {
			  $(this).addClass('loaded');
        $(window).resize();
			});
		},
		
		is_in_view: function(element) { 
			
			// Check if element is display none of a parent has display none
			if( $(element).is(':visible') == false ) {
				return false;
			}

			var _self = this;
			
			var window_top = $(window).scrollTop();
			
			var window_base = window_top + $(window).height();
			
			var element_top = $(element).offset().top;
			
			var element_base = element_top + $(element).height();
			
			// console.log(
			// 	'wn top: ' + window_top,
			// 	'wn base: ' + window_base,
			// 	'el top: ' + element_top,
			// 	'el base: ' + element_base,
			// 	'result: '+ (element_top <= window_base)
			// 	);
			
			_self.options.part_load = true

			if(_self.options.part_load === true) { 
				
				
				
				//Just top need to be visible etc
				return (element_top <= window_base + offset);
			}
			else {
				
				//Entire element needs to be visible
				return ((element_base <= window_base) && (element_top >= window_top));
			}
		}
	};
	// A really lightweight plugin wrapper around the constructor, preventing against multiple instantiations
	$.fn[pluginName] = function(options) {
		if (!$.data(this, "plugin_" + pluginName)) {
			
			$.data(this, 'plugin_' + pluginName, new Plugin(this, options));
		
		}
	}
})( jQuery, window, document );