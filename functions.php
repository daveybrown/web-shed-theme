<?php
/**
 * webshed functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/ 
 */
defined('ABSPATH') or die("");

define('HME_URL', get_home_url() );
define('ASS_URI', get_template_directory_uri() . '/assets');
define('TPL_URI', get_template_directory_uri() );
define('TPL_DIR', get_template_directory() );
define('UPL_URI', wp_upload_dir()['url'] );
define('UPL_PTH', wp_upload_dir()['path'] );
define('PIN_URI', plugins_url() );

include_once 'inc/acf-options.php';
include_once 'inc/analytics_tracking.php';
include_once 'inc/cache-js.php';
include_once 'inc/cache-bust.php';
include_once 'inc/lazyfish-wrap.php';
include_once 'inc/wp-images.php';
include_once 'inc/wp-no-emojis.php';
include_once 'inc/wp-options.php';
include_once 'inc/wp-menu.php';

/*
	Dbug function
 */

function dbug($object) {	
	print '<pre>';
	var_dump($object);
	print '</pre>';
}


/**
 * Filter the "read more" excerpt string link to the post.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
    return sprintf( '... <a class="read-more" href="%1$s">%2$s</a>',
        get_permalink( get_the_ID() ),
        __( 'read more', 'textdomain' )
    );
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );


function url_to_path ($url) {
	$path = str_replace( get_site_url() . '/', ABSPATH, $url);
	return $path;
}
function path_to_url ($path) {
	$url = str_replace( ABSPATH, get_site_url() . '/', $path);
	return $url;
}

/**
 * Proper way to enqueue scripts and styles.
 */
function wpdocs_theme_name_scripts() {
  wp_dequeue_style( 'contact-form-7' );  
	wp_deregister_script('jquery');
	if (SITE_COMPRESSION) {
	  wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/dist/export.min.js', null, cache_hash('/assets/js/dist/export.min.js'), true );
	  wp_deregister_script( 'jquery-form' );
	  wp_deregister_script( 'wp-embed' );
	}
}
add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );

// Used for copyright notice in footer
function auto_copyright($year = 'auto'){ 
   if(intval($year) == 'auto'){ $year = date('Y'); } 
   if(intval($year) == date('Y')){ echo intval($year); } 
   if(intval($year) < date('Y')){ echo intval($year) . ' - ' . date('Y'); }
   if(intval($year) > date('Y')){ echo date('Y'); }
}

// strip all comments from html
function callback($buffer) {
    $buffer = preg_replace('/<!--(.|s)*?-->/', '', $buffer);
    return $buffer;
}
function buffer_start() {
    ob_start("callback");
}
function buffer_end() {
    ob_end_flush();
}
if(!WP_DEBUG) :	
	add_action('get_header', 'buffer_start');
	add_action('wp_footer', 'buffer_end');
endif;

add_theme_support( 'post-thumbnails' );