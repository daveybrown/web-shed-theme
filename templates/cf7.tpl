<label><span class="show-for-sr">Your Name (required)</span>[text* your-name placeholder "Your name"] </label>

<!-- <label><span class="show-for-sr">Your Company</span>[text your-company placeholder "Your company (optional)"] </label> -->

<label><span class="show-for-sr">Your Email (required)</span>[email* your-email  placeholder "Your email address"] </label>

<!-- <label><span class="show-for-sr">Subject</span>[text your-subject placeholder "Message subject"] </label> -->

<label><span class="show-for-sr">Your Message (required)<</span>[textarea* your-message x3 placeholder "Your message"] </label>

[recaptcha]

<div class="wrap--submit">
  <div class="loader"></div>
  [submit class:button "Send"]  
</div>
