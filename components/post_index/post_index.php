<?php

  // dbug($this_module);

?>

<section class="post-index painted" id="<?php echo $this_module["module_id"]; ?>">
  
  <div class="row small-12 columns">
    <h1>
      <?php echo $this_module["post_index_heading"]; ?>
    </h1>
  </div>
  
  
  <?php 
  $args = array('post_type' => 'post', 'posts_per_page'=> -1);

  // the query
  $the_query = new WP_Query( $args ); ?>

  <?php if ( $the_query->have_posts() ) : ?>

    <div class="row">  
      
    <?php $counter = 1; ?>  
  	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
  		
      <?php if ( ($counter - 1) % 3 == 0) { ?>
        </div>
        <div class="row">
      <?php } ?>
      
      <div class="latest-post medium-4 columns <?php if ($counter == $the_query->post_count) {echo 'end';} ?>">
        <h2><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h2>
        <?php the_excerpt(); ?>
      </div>
      
      <?php $counter++; ?>  
  	<?php endwhile; ?>

  	<?php wp_reset_postdata(); ?>

    </div><!-- /.row -->

  <?php endif; ?>
  

</section>



