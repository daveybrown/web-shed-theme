<article class="page-post-content painted" id="<?php echo $this_module["module_id"]; ?>">

  <div class="row">    

    <div class="medium-12 columns">
      
      <h1 class="article__heading"><?php the_title(); ?></h1>
      
      <time pubdate datetime="<?php the_time('Y-m-d\TH:i:s'); ?>" class="article__date">
        <?php echo the_time('d/m/Y', null, null, false); ?>
      </time>
      
      <?php the_content(); ?>
    </div>

  </div><!-- /.row -->

</article>



