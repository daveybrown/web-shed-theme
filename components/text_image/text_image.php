<?php

  // dbug($this_module);

  $class['text'] = 'medium-12';
  $class['image'] = 'medium-12';
  if ( $this_module["text_image_text"] ) { 
    $class['image'] = 'medium-6';
  } 
?>

<section class="text-image painted" id="<?php echo $this_module["module_id"]; ?>">
  
  <div class="row small-12 columns">
    <h1>
      <?php echo $this_module["text_image_heading"]; ?>
    </h1>
  </div>

  <div class="row wrap--eh eh--medium">    
    
    <?php if ( $this_module["text_image_image"] ) { ?>
      <?php $class['text'] = 'medium-6'; ?>      
      <div class="medium-6 columns">
        <?php echo lazy_image( $this_module["text_image_image"]['sizes']['large'], $this_module["text_image_image"]['alt'] ); ?>     
      </div>
    <?php } ?>
    
    <?php if ( $this_module["text_image_text"] ) { ?>  
      <div class="<?php echo $class['text']; ?> columns text-switch">  
        <?php echo $this_module["text_image_text"]; ?>
      </div>
    <?php } ?>
    
  </div><!-- /.row -->

</section>



