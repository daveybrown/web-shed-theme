<div class="browser-mockup with-tab">
  
  <ul class="tabs" data-tabs id="portfolio-browser-tabs-<?php echo $module_number; ?>-<?php echo $portfolio_index; ?>">
    <?php $counter['browser_tabs'] = 1; ?>             
    <?php foreach ($portfolio_field['browser_tabs'] as $browser_tabs ) { ?>             
       <li class="tabs-title <?php if ($counter['browser_tabs'] == 1) echo 'is-active'; ?>">
         <a 
         href="#portfolio-browser-panel-<?php echo $module_number; ?>-<?php echo $portfolio_index; ?>-<?php echo $counter['browser_tabs']; ?>" 
         aria-selected="<?php if ($counter['browser_tabs'] == 1) echo 'true'; ?>" 
         title="<?php if ($browser_tabs['browser_tab_label_alt'])  { echo $browser_tabs['browser_tab_label_alt']; } else { echo $browser_tabs['browser_tab_label']; } ?>">
           <?php echo $browser_tabs['browser_tab_label']; ?>
         </a>
       </li>
       <?php $counter['browser_tabs']++; ?>  
    <?php } ?>
  </ul>
  
  <div class="tabs-content" data-tabs-content="portfolio-browser-tabs-<?php echo $module_number; ?>-<?php echo $portfolio_index; ?>">  
    <?php $counter['browser_tabs'] = 1; ?>             
    <?php foreach ($portfolio_field['browser_tabs'] as $browser_tabs ) { ?>             
       <div 
       class="tabs-panel <?php if ($counter['browser_tabs'] == 1) echo 'is-active'; ?> antiscroll-wrap" 
       id="portfolio-browser-panel-<?php echo $module_number; ?>-<?php echo $portfolio_index; ?>-<?php echo $counter['browser_tabs']; ?>" >
         <div class="box">
           <div class="antiscroll-inner">
             <div class="box-inner">
               <?php $image = $browser_tabs['browser_tab_image']; ?>
               <?php echo lazy_image( $image['sizes']['medium_large'], '' ); ?>
             </div>
           </div>
         </div>
       </div>
       <?php $counter['browser_tabs']++; ?>  
    <?php } ?>          
  </div><!-- /.tabs-content -->
  
  
</div><!-- /.browser-mockup -->