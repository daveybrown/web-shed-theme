<div class="editor-mockup with-tab">
  
  <ul class="tabs" data-tabs id="portfolio-editor-tabs-<?php echo $module_number; ?>-<?php echo $portfolio_index; ?>">
    <?php $counter['editor_tabs'] = 1; ?>             
    <?php foreach ($portfolio_field['editor_tabs'] as $editor_tabs ) { ?><li             
       class="tabs-title <?php if ($counter['editor_tabs'] == 1) echo 'is-active'; ?>">
         <a 
         href="#portfolio-editor-panel-<?php echo $module_number; ?>-<?php echo $portfolio_index; ?>-<?php echo $counter['editor_tabs']; ?>" 
         aria-selected="<?php if ($counter['editor_tabs'] == 1) echo 'true'; ?>" 
         title="<?php if ($editor_tabs['editor_tab_label_alt'])  { echo $editor_tabs['editor_tab_label_alt']; } else { echo $editor_tabs['editor_tab_label']; } ?>">
           <?php echo $editor_tabs['editor_tab_label']; ?>
         </a>
       </li><?php 
       $counter['editor_tabs']++;
      //  dbug($editor_tabs);
     } ?>
  </ul>
  
  
  <div class="tabs-content" data-tabs-content="portfolio-editor-tabs-<?php echo $module_number; ?>-<?php echo $portfolio_index; ?>">
    <?php $counter['editor_tabs'] = 1; ?>
    <?php foreach ($portfolio_field['editor_tabs'] as $editor_tabs ) { ?>  
       <?php if ($editor_tabs['editor_tab_lang'])  { $lang = pathinfo($editor_tabs['editor_tab_lang'], PATHINFO_EXTENSION); } else { $lang = 'html'; } ?>">           
       <div 
       class="tabs-panel <?php if ($counter['editor_tabs'] == 1) echo 'is-active'; ?> antiscroll-wrap" 
       id="portfolio-editor-panel-<?php echo $module_number; ?>-<?php echo $portfolio_index; ?>-<?php echo $counter['editor_tabs']; ?>" >
         <div class="box">
           <div class="antiscroll-inner">
             <div class="box-inner">
               <pre ><code class="<?php echo $lang; ?>"><span><?php echo htmlentities($editor_tabs['editor_tab_content']); ?></span></code></pre>
             </div>
           </div>
         </div>
       </div>
       <?php $counter['editor_tabs']++; ?>  
    <?php } ?>
  </div>
  
</div><!-- /.editor-mockup -->