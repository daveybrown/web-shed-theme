<section class="hero" id="<?php echo $this_module["module_id"]; ?>">
  
  <div class="row small-12 medium-10 large-8 columns text-left">
    <h1 class="Nbg--paint">
      <?php echo $this_module["hero_heading"]; ?>
    </h1>
    <?php if($this_module["hero_text"]) : ?>
      <div class="clear"></div>
      <div class="wysiwyg Nbg--paint Nbg--paint--2">
        <?php echo $this_module["hero_text"]; ?>
      </div>
    <?php endif; ?>
  </div><!-- /.row -->

</section>



