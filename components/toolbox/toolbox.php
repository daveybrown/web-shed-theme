<?php if( $this_module['posts_tool'] ): ?>
	<?php foreach( $this_module['posts_tool'] as $post_tool ) : ?>
		<?php if ($post_tool['post_tool']) : ?>
      <?php $toolbox_fields[] = get_fields($post_tool['post_tool']->ID); ?>
    <?php endif; ?>  
	<?php endforeach; ?>
<?php endif; ?>

<section class="toolbox toolbox--<?php echo $module_number; ?> painted" id="<?php echo $this_module["module_id"]; ?>">
  
  <div class="row small-12 columns">
    <h1>
      Toolbox
    </h1>
  </div>
  
	<div class="row">
    <div class="large-8 medium-10 small-centered columns wysiwyg-content">
			<p>
				I use the majority of these tools the majority of the time, in fact I've used 19 of them on this website. 
				These tools help me make <strong>better websites faster</strong>; if it doesn't do that then, it's not on the list! 
				Hover or tap the icons to find out why I use each one.
			</p>
  	</div>
  </div>
	
	
  <div class="row row--toolbox" id="toolbox-tabs-<?php echo $module_number; ?>" data-equalizer>
		
		<?php $html_tooltip = ''; ?>
  
  	<?php foreach ($toolbox_fields as $toolbox_index => $toolbox_field) { ?>
  		
  		
  		<?php
      $has_tooltip = false;

			if( isset($toolbox_field['tool_tips']) && $toolbox_field['tool_tips'] ) {		
				$has_tooltip =  true;		
				$html_tooltip .= "<span id='tooltip_content-{$module_number}-{$toolbox_index}' data-show='0'>";
				foreach ($toolbox_field['tool_tips'] as $tool_tip_index => $tool_tip) {
					$html_tooltip .= "<span class='tip--part tip--part-{$tool_tip_index}'>{$tool_tip['tool_tip']}</span>";					
				}				
				$html_tooltip .= "</span>";	
			}
			
      // dbug($toolbox_field); 
      
      // echo $toolbox_field['tool_copy'];
			$src_fill = false;
      $src = $toolbox_field['tool_image']['url'];
      if (isset($toolbox_field['tool_image_svg_fill']) && $toolbox_field['tool_image_svg_fill'] !== '' && $toolbox_field['tool_image']['mime_type'] == 'image/svg+xml') : 
        $src_fill = UPL_URI . '/svg.php?fill='.$toolbox_field['tool_image_svg_fill'].'&file=' . basename($src);
        // $toolbox_fields[$toolbox_index]['tool_image']['url'] = $src_fill;
      endif;  
      
      ?>
      	
				<div
        class="large-2 medium-4 small-6 columns">
          <span
          class="wrap--logo <?php if ($has_tooltip) { echo 'has--tooltip'; } ?>"
					data-tooltip-content="#tooltip_content-<?php echo $module_number; ?>-<?php echo $toolbox_index; ?>"
					data-tooltip-content-class="tooltip_content-<?php echo $module_number; ?>-<?php echo $toolbox_index; ?>"
          data-equalizer-watch
					>
						<?php 						
						if ($src_fill) {
							echo (str_replace ( $src, $src_fill, lazy_image( $src, '' )));
						} else {
							echo  lazy_image( $src, '' ) ;
						} 
						?>
          </span>
  	    </div>
				
				<?php /*
				
        <div
        class="large-2 medium-4 small-6 columns tabs-title <?php if ($toolbox_index === 0) echo 'is-active'; ?>"
        aria-selected="<?php if ($toolbox_index === 0) echo 'true'; ?>">
          <a 
          href="#toolbox-panel--<?php echo $module_number; ?>-<?php echo $toolbox_index; ?>" 
          class="wrap--logo <?php if ($has_tooltip) { echo 'has--tooltip'; } ?>"
					data-tooltip-content="#tooltip_content-<?php echo $module_number; ?>-<?php echo $toolbox_index; ?>"
					data-tooltip-content-class="tooltip_content-<?php echo $module_number; ?>-<?php echo $toolbox_index; ?>"
          data-equalizer-watch
					>
						<?php 						
						if ($src_fill) {
							echo (str_replace ( $src, $src_fill, lazy_image( $src, '' )));
						} else {
							echo  lazy_image( $src, '' ) ;
						} 
						?>
          </a>
  	    </div>
				
				*/ ?>
  	
  	<?php } ?>
  
  </div><!-- /.row -->
	
	<?php /*
	
  <div class="tabs-content" data-tabs-content="toolbox-tabs-<?php echo $module_number; ?>">

  
  	<?php foreach ($toolbox_fields as $toolbox_index => $toolbox_field) { ?>
  		
        <div
        class="tabs-panel <?php if ($toolbox_index === 0) echo 'is-active'; ?>" 
        id="toolbox-panel--<?php echo $module_number; ?>-<?php echo $toolbox_index; ?>">
          
          <div class="row">
          
            <div class="small-4 columns">
							<?php echo lazy_image( $toolbox_field['tool_image']['url'], '' ); ?>
            </div>
            <div class="small-8 columns">
              <?php if (isset($toolbox_field['tool_copy'])) { echo $toolbox_field['tool_copy']; } ?>
            </div>
        
          </div>
          
        </div>
  	
  	<?php } ?>
  
  </div><!-- /.row -->
	
	*/ ?>


  <div class="tooltip_templates">
	    <?php echo $html_tooltip; ?>
	</div>
	
	<div class="row">
    <div class="large-8 medium-10 small-centered columns wysiwyg-content">
			<p>
				This is an evolving list, as I'm always looking to improve my workflow. So don't be surprised to see new additions or changes.
			</p>
  	</div>
  </div>
	
</section>