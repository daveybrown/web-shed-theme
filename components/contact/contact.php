<section class="contact" id="<?php echo $this_module["module_id"]; ?>"> 

  <div class="row small-12 columns">
    <h1 id="tooltip-test">
      <?php echo $this_module["contact_heading"]; ?>
    </h1>
  </div>

  <div class="row row--contact-form">    
    <div class="medium-12 columns">
      <?php echo do_shortcode($this_module["contact_shortcode"]); ?>
    </div>
  </div><!-- /.row -->

</section>