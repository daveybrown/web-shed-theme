<?php $show_more_button = false; ?>
<?php if( $this_module['posts_work'] ): ?>
	<?php foreach( $this_module['posts_work'] as $post_work ): ?>
    <?php $portfolio_fields[] = get_fields($post_work['post_work']->ID); ?>
	<?php endforeach; ?>
<?php endif; ?>

<section class="portfolio portfolio--<?php echo $module_number; ?> painted" id="<?php echo $this_module["module_id"]; ?>">
  
  <div class="row small-12 columns">
    <h1>
      Portfolio
    </h1>
  </div>
	
  <div class="row">
    <div class="large-8 medium-10 small-centered columns wysiwyg-content">
			<p>
				For the past year I’ve been working in Angel, London as part of the cookie and coffee loving team that is Pixeled Eggs. I’ve worked on exciting builds such as the Rochambeau Smart Jacket for evrythng.com and high profile websites for MacMillan (Game Changers and Summer Lights). 
			</p>
			<p>
				The work that I've done at Pixeled Eggs varies from building complete websites from scratch to completing additional phases of work on existing websites.
			</p>
			<p>
				In many cases I worked as part of a team with other developers, usually 2 splitting the workload 50/50, or working on websites where I was responsible for everything. Most projects were agile; with progress discussed in daily stand-ups and tracked online with Jira.
			</p>
			<p>
				I’ve also worked on several freelance projects including websites for charities and small businesses. 
			</p>
			<p>
				Below are a few examples, check out the tabs for a preview of what my work looks like<span class="show-for-medium"> and some of the code behind the scenes</span>. 
			</p>
  	</div>
  </div>
  
	<?php foreach ($portfolio_fields as $portfolio_index => $portfolio_field) { ?>
		
		<?php if (isset ($portfolio_field["work_heading"]) ) : ?>
			
			<div class="row">
				<div class="large-8 medium-10 small-centered columns wysiwyg-content">
					<?php echo $portfolio_field["work_heading"]; ?>
				</div>
			</div>
			
		<?php endif; ?>
		
	  <div class="row <?php if ($show_more_button == true && $portfolio_index == 1) { echo 'row--more'; } ?>">    
	    <div class="medium-6 small-11 small-centered medium-uncentered centered-fix columns">      
	      <?php include(TPL_DIR . '/components/browser_mockup/portfolio_browser.php'); ?>      
	    </div>
	    
	    <div class="medium-6 small-11 small-centered medium-uncentered centered-fix show-for-medium last columns">      
	      <?php include(TPL_DIR . '/components/browser_mockup/portfolio_editor.php'); ?>
	    </div>
	  </div><!-- /.row -->
		
		<?php if (isset ($portfolio_field["work_description"]) ) : ?>
			
			<div class="row">
				<div class="large-8 medium-10 small-centered columns wysiwyg-content">
					<?php echo $portfolio_field["work_description"]; ?>
				</div>
			</div>
			
		<?php endif; ?>
	
	<?php } ?>
	
	<?php if ($show_more_button == true && count($portfolio_fields) > 1) : ?>
		<div class="row">
			<div class="small-12 columns text-center">
				<button class="button button--more" data-toggle-class="more" data-target=".portfolio--<?php echo $module_number; ?>" >
					<span class="show-for-sr">Toggle more work</span>
					<span class="text--more" aria-hidden="true">See more work</span>
					<span class="text--less" aria-hidden="true">See less work</span>
				</button>
			</div>
		</div>
	<?php endif; ?>
	
</section>