<?php 

$split['c'] = count($this_module['spaces']);


foreach ($this_module['spaces'] as $i => $space) {
  
  $split['b'] = ''; // before
  $split['a'] = ''; // after
  
  $split['x'] = $module_number + $i+1;
  
  $split['a'] .= "</div><!-- /.columns -->";
  
  if ($i == 0) {
    $split['b'] .= "<div class='wrap--split painted' id='split--{$this_module['split_id']}'><div class='row row--split'>";
  } elseif ( ($i + 1)  == $split['c']) {
    $split['a'] .= "</div><!-- /.row--split -->";
    $brush_section = '';
    $split['a'] .= str_replace ( '<?php echo ASS_URI; ?>' , ASS_URI , $brush_section );
    $split['a'] .= "</div><!-- /.wrap--split -->";
  }
  
  $split['b'] .= "<div class='{$space['space_class']} columns'>";
  
  $module_modify[$split['x']]['html']['before'] = $split['b'];
  $module_modify[$split['x']]['html']['after'] = $split['a'];  
  
}
