<?php 
$args = array('post_type' => 'post', 'posts_per_page'=> 3);

// the query
$wp_query = new WP_Query( $args ); ?>

<?php if ( $wp_query->have_posts() ) : ?>
  
<section class="latest-posts painted" id="<?php echo $this_module["module_id"]; ?>">

  <div class="row small-12 columns">
    <h1>
      Blog
    </h1>
  </div>

  <div class="row">  
    
  <?php $counter = 1; ?>  
	<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
		
    <div class="latest-post medium-4 columns <?php if ($counter == $the_query->post_count) {echo 'end';} ?>">
      <h2><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h2>
      <?php the_excerpt(); ?>
    </div>
    
    <?php $counter++; ?>  
	<?php endwhile; ?>

  </div><!-- /.row -->
  
  <div class="row">  
    <div class="columns text-center">
      <?php if (get_previous_posts_link() || get_next_posts_link()) { ?>
          <a href="<?php the_permalink(get_option('page_for_posts')); ?>" class="button">See all blogs</a>
      <?php } ?>
    </div>
  </div><!-- /.row -->
  
	<?php wp_reset_postdata(); ?>

</section>

<?php endif; ?>

  

    

