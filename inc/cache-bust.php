<?php
function cache_bust($file) {
  $path = TPL_DIR . '/' . $file;
  $hash = hash_file('md5', $path);
  $src = TPL_URI . '/' . $file . '?' . $hash;
  return $src;  
}
function cache_hash($file) {
  $path = TPL_DIR . '/' . $file;
  $hash = hash_file('md5', $path);
  return $hash;  
}