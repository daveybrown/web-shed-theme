<?php
defined('ABSPATH') or die("");

// cache_js();

// Filter to change url of analytics js from google to local
add_filter( 'aiosp_google_analytics', 'filter_aioseop_ga', 10, 1 );
function filter_aioseop_ga( $analytics ){
  $return = str_replace ( '//www.google-analytics.com/analytics.js' , TPL_URI . '/assets/js/local/local-ga.js' , $analytics );
  return $return;
}


// This script creates a local copy of external resources and checks for updates daily
cache_js_schedule();

function cache_js_schedule(){
  //Use wp_next_scheduled to check if the event is already scheduled
  $timestamp = wp_next_scheduled( 'cache_js_daily' );
  //If $timestamp == false schedule daily backups since it hasn't been done previously
  if( $timestamp == false ){
    //Schedule the event for right now, then to repeat daily using the hook 'cache_js'
    wp_schedule_event( time(), 'daily', 'cache_js_daily' );
  }
}

//Hook our function , wi_create_backup(), into the action cache_js
add_action( 'cache_js_daily', 'cache_js' );

// Run manually
// cache_js();

function cache_js(){   
  // Doesn't work well with fb. 
  // $resource = array ( 'https://www.google-analytics.com/analytics.js' => TPL_DIR . '/assets/js/local/local-ga.js',
  //                     'https://platform.twitter.com/widgets.js' => TPL_DIR . '/assets/js/local/local-twitter.js',
  //                     'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8&appId=' . FB_APP_ID => TPL_DIR . '/assets/js/local/local-fb.js'); 
                       
  $resource = array ( 'https://www.google-analytics.com/analytics.js' => TPL_DIR . '/assets/js/local/local-ga.js');  
  
  foreach ($resource as $url => $path) {
    save_ext( $url, $path );
  }
}

// script to update local version of Google analytics script

function save_ext( $url, $path  ) {
  // Remote file to download
  $remoteFile = $url;
  $localfile = $path;
  //For Cpanel it will be /home/USERNAME/public_html/local-ga.js

  // Connection time out
  $connTimeout = 10;
  $url = parse_url($remoteFile);
  $host = $url['host'];
  $path = isset($url['path']) ? $url['path'] : '/';

  if (isset($url['query'])) {
   $path .= '?' . $url['query'];
  }

  $port = isset($url['port']) ? $url['port'] : '80';
  $fp = @fsockopen($host, '80', $errno, $errstr, $connTimeout );
  if(!$fp){
   // On connection failure return the cached file (if it exist)
   if(file_exists($localfile)){
   readfile($localfile);
   }
  } else {
   // Send the header information
   $header = "GET $path HTTP/1.0\r\n";
   $header .= "Host: $host\r\n";
   $header .= "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6\r\n";
   $header .= "Accept: */*\r\n";
   $header .= "Accept-Language: en-us,en;q=0.5\r\n";
   $header .= "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7\r\n";
   $header .= "Keep-Alive: 300\r\n";
   $header .= "Connection: keep-alive\r\n";
   $header .= "Referer: http://$host\r\n\r\n";
   fputs($fp, $header);
   $response = '';

  // Get the response from the remote server
   while($line = fread($fp, 4096)){
   $response .= $line;
   }

  // Close the connection
   fclose( $fp );

  // Remove the headers
   $pos = strpos($response, "\r\n\r\n");
   $response = substr($response, $pos + 4);

  // Return the processed response
  //  echo $response;

  // Save the response to the local file
   if(!file_exists($localfile)){
   // Try to create the file, if doesn't exist
   fopen($localfile, 'w');
   }

  if(is_writable($localfile)) {
   if($fp = fopen($localfile, 'w')){
   fwrite($fp, $response);
   fclose($fp);
   }
   }
  }
}


?>