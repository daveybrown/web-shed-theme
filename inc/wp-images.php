<?php

defined('ABSPATH') or die("");

// Create custom image sizes
// Gallery image on homepage
add_action( 'after_setup_theme', 'wpdocs_theme_setup' );
function wpdocs_theme_setup() {
    // Your image sizes here
    add_image_size( 'browser_tab_image', 640, 9999999 );
}

// Allow svg uploads to media library
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');