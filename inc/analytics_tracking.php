<?php

defined('ABSPATH') or die("");

/**
 * Add Google Analytics code to header
 */
 
function add_google_analytics() {
$id = ANALYTICS_ID;
$ass_uri = ASS_URI;
$html = <<< EOT
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','$ass_uri/js/local/local-ga.js','ga');

	ga('create', '$id', 'auto');
	ga('send', 'pageview');
</script>
EOT;
echo $html;
}
if (ANALYTICS_ID) {
  add_action('wp_head', 'add_google_analytics');
}


/**
 * Add Google Tag Manager code to footer
 */

function add_googletagmanager() {
$id = GOOGLE_TAG_ID;
$html = <<< EOT
<script>
	(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','$id');
</script>
EOT;
echo $html;
}
if (GOOGLE_TAG_ID) {
	// add_action('wp_footer', 'add_googletagmanager');
}