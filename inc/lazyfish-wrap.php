<?php 
function lazy_image($url, $alt = '') {
	
	$width = false;
	$height = false;
	$vb = false;
	$attr = '';
	$path = str_replace( get_site_url() . '/', ABSPATH, $url);
	
	if(file_exists($path)){
		
		if (substr($url, -4) == '.svg') {
			// its an svg, todo
			// gets a bit tricky because of svg.php
			$svgfile = simplexml_load_file($path);
			$xmlattributes = $svgfile->attributes();
			
			if ( isset($xmlattributes->viewBox) ) {
				$vb = (string) $xmlattributes->viewBox;
			} elseif ( isset($xmlattributes->viewbox) ) {
				$vb = (string) $xmlattributes->viewbox;
			} 
			if ( isset($xmlattributes->width) && isset($xmlattributes->height) )  {
				$width = (string) $xmlattributes->width; 
				$height = (string) $xmlattributes->height;
			}
		} else {
			list($width, $height) = getimagesize($path);			
		}
		
	}
	
	if ($vb) {
		$attr = "viewBox='$vb' ";
	}	
	
	if ($width) {
		$attr .= "width='$width' ";
	}	
	
	if ($height) {
		$attr .= "height='$height' ";
	}	
	
	
	// Use svg rectangle to get correct size for placeholedr image
	$svg = "data:image/svg+xml,%3Csvg $attr xmlns='http://www.w3.org/2000/svg'%3E\a \a \a%3C/svg%3E";		

	
	$html  = '<div class="lazy" >';
	$html .= '<img class="img--real" data-src="' . $url . '" alt="' . $alt . '" />';
	$html .= '<img class="img--dummy" src="'.$svg.'"  />';
	$html .= '</div>';	
	
	return $html;
}