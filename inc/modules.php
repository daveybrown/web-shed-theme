<?php

  $module_array = [
    'comments',
    'contact',
    'hero',
    'latest_posts',
    'page_post_content',
    'post_index',
    'portfolio',
    'split_module',
    'text_image',
    'toolbox'
  ];
  
  if (isset($fileds["modules"]) && $modules = $fileds["modules"]) :
    
    // loop through the rows of data
    foreach ( $modules as $module_number => $module  )	:

      if( in_array ( $module["acf_fc_layout"] , $module_array )):	    
        
        $this_module = $module;
        
        if ( isset($module_modify[$module_number]['html']['before']) ) {
          echo $module_modify[$module_number]['html']['before'];  
        }
         	
        include TPL_DIR . '/components/' . $module["acf_fc_layout"] . '/' . $module["acf_fc_layout"] . '.php';
        
        if ( isset($module_modify[$module_number]['html']['after']) ) {
          echo $module_modify[$module_number]['html']['after'];  
        }

      endif;
      
      $previous_module = $module["acf_fc_layout"];
       
    endforeach;
    
  endif;

?>