<?php if(SITE_COMPRESSION) : ?>
  <script type="text/javascript">
    <?php include( get_template_directory() . '/assets/js/dist/export-fonts.min.js'); ?>
  </script>
  <style type="text/css">
    <?php echo str_replace ( 'url("..', 'url("' . ASS_URI , file_get_contents( get_template_directory() . '/assets/css/fonts-inline.min.css') ); ?>
  </style>
<?php else : ?>
  <script type="text/javascript" src="<?php echo ASS_URI; ?>/js/js.cookie.js"></script>
  <script type="text/javascript" src="<?php echo ASS_URI; ?>/js/webfontloader.js"></script>
  <style type="text/css">
    <?php echo str_replace ( 'url("..', 'url("' . ASS_URI , file_get_contents( get_template_directory() . '/assets/css/fonts-inline.css') ); ?>
  </style>
<?php endif; ?>

<script type="text/javascript">

if ( Cookies.get('Ubuntu-n4') ) {
  var h = document.getElementById("html");
  h.classList.add("wf-ubuntu-n4-cached");
} 
if ( Cookies.get('Paytone One-n4') ) {
  var h = document.getElementById("html");
  h.classList.add("wf-paytoneone-n4-cached");
} 
WebFontConfig = {
   fontactive: function(familyName, fvd) { 
     Cookies.set(familyName + '-' + fvd, 'loaded', { expires: 365 });
   },
   custom: {
     families: ['Ubuntu:n4,n7,i4,i7', 'Paytone One:n4']
   },
	 timeout: 2000 // Set the timeout to two seconds
 };
WebFont.load(WebFontConfig);
  
</script>		 