<?php

defined('ABSPATH') or die("");

/**
 * Add Options Pages
 */
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Site Options',
		'menu_title'	=> 'Site Options',
		'menu_slug' 	=> 'options',
		'capability'	=> 'edit_posts',
		'redirect'	=> false
	));
	
}