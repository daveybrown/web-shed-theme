<?php

defined('ABSPATH') or die("No direct access");

/**
 * Shows Menu in WP Admin
 */
register_nav_menu('primary', __('Primary Menu'));

/* ========================================================================== */
/* ========================================================================== */

//Replaces "current-menu-item" with "selected"
function current_to_active($text) {
	$replace = array(
			//List of menu item classes that should be changed to "selected"
			'current_page_item' => 'selected',
			'current_page_parent' => 'selected',
			'current-page-ancestor' => 'selected',
	);
	$text = str_replace(array_keys($replace), $replace, $text);
	return $text;
}

add_filter('wp_nav_menu', 'current_to_active');

/* ========================================================================== */
/* ========================================================================== */

//Deletes empty classes and removes the sub menu class
function strip_empty_classes($menu) {
    $menu = preg_replace('/ class=""| class="sub-menu"/','',$menu);
    return $menu;
}

add_filter ('wp_nav_menu','strip_empty_classes');

/* ========================================================================== */
/* ========================================================================== */

function special_nav_class($classes, $item){
    if( in_array('current-menu-item', $classes) ){
             $classes[] = 'active ';  // your new class
     }
     return $classes;
}

//add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

/* ========================================================================== */
/* ========================================================================== */

//Deletes all CSS classes and id's, except for those listed in the array below
function custom_wp_nav_menu($var) {
	
	//List of allowed menu classes
	
	return is_array($var) ? array_intersect($var, array(		
		'current_page_item',
		'current_page_parent',
		'current-page-ancestor',
		'first',
		'last',
		'vertical',
		'horizontal',
		'active'
		)
	) : '';
}

add_filter('nav_menu_css_class', 'custom_wp_nav_menu');

add_filter('nav_menu_item_id', 'custom_wp_nav_menu');

add_filter('page_css_class', 'custom_wp_nav_menu');

add_filter('show_admin_bar', '__return_false');

